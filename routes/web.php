<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Pages
Route::get('/', 'Frontend\PagesController@home')->name('home');
Route::get('pages/{name}', 'Frontend\PagesController@show')->name('pages');
Route::post('pages/contact', 'Frontend\PagesController@send')->name('contact.send');


Auth::routes();


// Admin
Route::group(['prefix'=>'admin','middleware'=>'auth'],function(){
	
	//Analytics
	Route::get('/', 'Backend\AnalyticsController@index')->name('analytics');

	//Pages
	Route::resource('/pages','Backend\PagesController');


	//Sliders
	Route::resource('/sliders','Backend\SlidersController');

	//Slide Item
	Route::put('/slides/{slide_item}','Backend\SlidersController@update_slide')->name('slides.update');

    //Photos
	Route::resource('/photos','Backend\PhotosController');


});	



