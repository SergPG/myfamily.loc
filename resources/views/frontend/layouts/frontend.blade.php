
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
	  <link rel="dns-prefetch" href="//fonts.gstatic.com">
	  <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Bootstrap -->
 	  <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
 	  <link href="{{asset('css/animate.css')}}" rel="stylesheet">
 	  <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">
 	  <link href="{{asset('css/jquery.bxslider.css')}}" rel="stylesheet">

    <link href="{{asset('css/isotope.css')}}" rel="stylesheet" media="screen">
    <link href="{{asset('js/fancybox/jquery.fancybox.css')}}" rel="stylesheet" media="screen">

 	<!-- Custom styles for this template -->
 	  <link href="{{asset('css/style.css')}}" rel="stylesheet">
   
  </head>

  <body>

  <!-- Main Navigation -->
    @section('main_navigation')
      @include('frontend.layouts.main_navigation')  
    @show
  <!-- END Main Navigation -->
 

<!-- Main Content -->
	@yield('content')
<!-- END Main Content -->


<!-- Footer -->
    @section('footer')
      @include('frontend.layouts.footer')  
    @show
  <!-- END Footer -->



 <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
 
  <script src="{{ asset('js/jquery-2.1.1.min.js') }}"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="{{asset('js/bootstrap.min.js')}}"></script>
  <script src="{{asset('js/wow.min.js')}}"></script>
  <script src="{{asset('js/jquery.easing.1.3.js')}}"></script>
  <script src="{{asset('js/jquery.bxslider.min.js')}}"></script>
  <script src="{{asset('js/jquery.isotope.min.js')}}"></script>
  <script src="{{asset('js/fancybox/jquery.fancybox.pack.js')}}"></script>

  <script src="{{asset('js/functions.js')}}"></script>  
  
  <script>
    wow = new WOW({}).init();
  </script>

</body>
	

     