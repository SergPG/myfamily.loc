<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse.collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.html"><span>Me & Family</span></a>
      </div>
      <div class="navbar-collapse collapse">
        <div class="menu">

           {!! $menuMain->asUl(['class' => 'nav nav-tabs','role'=>'tablist']); !!}
          {{-- <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="index.html">Home</a></li>
            <li role="presentation"><a href="ourstory.html">Our Story</a></li>
            <li role="presentation"><a href="events.html">Events</a></li>
            <li role="presentation"><a href="gallery.html">Gallery</a></li>
            <li role="presentation"><a href="contact.html">Contact</a></li>
          </ul> --}}
        </div>
      </div>
    </div>
  </nav>