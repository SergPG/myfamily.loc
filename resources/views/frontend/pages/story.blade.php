@extends('frontend.layouts.frontend')

@section('content')

   
  

  <div class="breadcrumb">
    @if(isset($page->heading))
      <h2>{{ $page->heading }}</h2>
    @endif
  </div>

  <div class="ourstory">
    <div class="container">
      <div class="col-md-10 col-md-offset-1">

   
      @if(isset($slide_items) && ($slide_items->count() > 1))
     <div class="slider">
    <div class="img-responsive">


      <ul class="bxslider">

        @foreach( $slide_items as $slide_item )
          <li><img  src="{{asset($slide_item->image) }} " alt="" /></li>
        @endforeach
    
      </ul>

 </div>
   </div>  <!--/.slider  -->

      @else
        
        @if(isset($slide_items) && ($slide_items->count() > 0))
        @foreach( $slide_items as $slide_item )
          <img  src="{{asset($slide_item->image) }} " alt="" class="img-responsive" />
        @endforeach
        @endif
      
      @endif  

      @if(isset($page->description))
       {!! $page->description !!} 
      @endif
        
      </div>
    </div>
  </div>


  <hr>
  <div class="container">
    <div class="col-md-4">
      <img src="{{asset('storage/img/1.jpg') }}" alt="" class="img-responsive" />
    </div>
    <div class="col-md-8">
      <h2>Jessa</h2>
      <p>
        praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint Voluptatem accusantium doloremque laudantium sprea totam rem aperiam praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint
        Voluptatem accusantium doloremque laudantium sprea totam rem aperiam praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint Voluptatem accusantium doloremque laudantium sprea totam rem aperiam praesentium
        voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint Voluptatem accusantium doloremque laudantium sprea totam rem aperiam
      </p>
    </div>
  </div>

  <hr>
  <div class="container">
    <div class="col-md-8">
      <h2>Jessica</h2>
      <p>
        praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint Voluptatem accusantium doloremque laudantium sprea totam rem aperiam praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint
        Voluptatem accusantium doloremque laudantium sprea totam rem aperiam praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint Voluptatem accusantium doloremque laudantium sprea totam rem aperiam praesentium
        voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint Voluptatem accusantium doloremque laudantium sprea totam rem aperiam
      </p>
    </div>
    <div class="col-md-4">
      <img src="{{asset('storage/img/2.jpg') }}" alt="" class="img-responsive" />
    </div>
  </div>

  <hr>
  <div class="container">
    <div class="col-md-4">
      <img src="{{asset('storage/img/3.jpg') }}" alt="" class="img-responsive" />
    </div>
    <div class="col-md-8">
      <h2>Scott</h2>
      <p>
        praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint Voluptatem accusantium doloremque laudantium sprea totam rem aperiam praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint
        Voluptatem accusantium doloremque laudantium sprea totam rem aperiam praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint Voluptatem accusantium doloremque laudantium sprea totam rem aperiam praesentium
        voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint Voluptatem accusantium doloremque laudantium sprea totam rem aperiam
      </p>
    </div>
  </div>
  <hr>
  <div class="profile">
    <div class="container">
      <div class="col-md-8">
        <h2>James</h2>
        <p>
          praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint Voluptatem accusantium doloremque laudantium sprea totam rem aperiam praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint
          Voluptatem accusantium doloremque laudantium sprea totam rem aperiam praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint Voluptatem accusantium doloremque laudantium sprea totam rem aperiam praesentium
          voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint Voluptatem accusantium doloremque laudantium sprea totam rem aperiam
        </p>
      </div>
      <div class="col-md-4">
        <img src="{{asset('storage/img/4.jpg') }}" alt="" class="img-responsive" />
      </div>
    </div>
  </div>
  <div class="container">
    <nav>
      <ul class="pagination">
        <li class="disabled"><a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
        <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
        <li><a href="#">2 <span class="sr-only">(current)</span></a></li>
        <li><a href="#">3 <span class="sr-only">(current)</span></a></li>
        <li><a href="#">4 <span class="sr-only">(current)</span></a></li>
      </ul>
    </nav>
  </div>
  
@endsection

		