@extends('backend.layouts.backend')

@section('content')


  <div class="content-wrapper">
    <div class="container-fluid">

      <!-- Breadcrumbs-->
       <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="{{ route('analytics')  }}">{{ __('Analytics') }}</a>
        </li>
        <li class="breadcrumb-item active">Photos</li>
      </ol>
      <!-- END Breadcrumbs-->
     
      <!-- Photos DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Data Table Photos
        </div>
        <div class="card-body">      

          <div class="table-responsive">

             <table id="examplePages" class="table table-hover table-bordered" width="100%" cellspacing="0"> 
             
            <thead class="thead-dark">
              <tr>
                <th class="d-none">ID</th>
                <th scope="col">Heading</th>
                <th scope="col">Name item Menu</th>
                <th scope="col">Order in Menu</th>
                <th scope="col" class="text-center">Actions</th>
              </tr>
            </thead>

            <tbody>
            @if(isset($pages) && ($pages->count()) > 0 )
              @foreach ($pages as $page)

              <tr id="page_{{$page->id}}">
                <td class="d-none"> {{ $page->id }} </td>
                <td width="30%" class="align-middle">  {{ $page->heading }} </td>
                <td width="30%" class="align-middle">  {{ $page->name }} </td> 
                <td width="20%"class="align-middle">  {{ $page->order_menu }} </td> 

               <td width="10%"> 
                  <a id="btnEdit" class="btn btn-success " href="{{ route('pages.edit',['page ' => $page]) }}" title="Edit" role="button"><i class="fa fa-edit fa-lg" aria-hidden="true"></i> Edit</a>
                </td>

              </tr>

              @endforeach

             @else 

              <tr>

                <td class="d-none">No Data</td>
                <td width="50%"> No Data</td>
                <td width="40%"> No Data</td>
                <td width="40%"> No Data</td>

                <td width="20%"> 
                  <a id="btnEdit" class="btn btn-success  disabled" href="" title="Edit" role="button"><i class="fa fa-edit fa-lg" aria-hidden="true"></i> Edit</a>
                </td>
                
             @endif 

             </tbody> 
           </table>

          </div>

        </div>
        <div class="card-footer small text-muted"> 
        Updated yesterday at 11:59 PM
        </div>   

      <meta name="_token" content="{!! csrf_token() !!}" />

      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
  
@endsection

    
@push('script_Pages')



  <script type="text/javascript">

// DESC  //  ASC
  $(document).ready(function(){
      
     
    //delete Slide and remove it from list
    $(document).on('click','#btnDelete',function(){
        
        var page_id = $(this).val();
        var url = $(this).data("url");

        console.log(url, page_id);
        
    //     debugger;
       
        if(confirm("ВЫ ХОТИТЕ УДАЛИТЬ ЗАПИСЬ "+ page_id + " ?"))
        {
             $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })
            $.ajax({
                type: "DELETE",
                url: url,
                processData : false,
                contentType : false,
                success: function (data) {
                    console.log(data);
                    $("#page_" + page_id).remove();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
         }; // end if
       });


    

  });
    
  </script>

  
@endpush

		