<div class="card my-2 mx-2" style="width: 18rem;">

  <img src="{{asset('storage/img/portfolio/2.jpg') }}" class="card-img-top" alt="...">

    <div class="card-body">
     <!-- <h5 class="card-title">Название карточки</h5>
            
      <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p> -->
            
      <div class="my-1 clearfix">

        <a id="btnEdit" class="btn btn-success float-left ml-3 disabled" href="" title="Edit" role="button"><i class="fa fa-edit fa-lg" aria-hidden="true"></i> Edit</a>

        <button  id="btnDelete" type="button" disabled class="btn btn-danger float-right mr-3"  title="Delete" value=""  data-url="" > <i class="fa fa-trash fa-lg" aria-hidden="true"></i> Delete </button>

      </div> <!-- /.btn clearfix -->

    </div> <!-- /.card-body -->
</div> <!-- /.card -->