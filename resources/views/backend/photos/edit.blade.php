@extends('backend.layouts.backend')

@section('content')

<div class="content-wrapper">
  <div class="container-fluid">


     <!-- Breadcrumbs-->
       <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="{{ route('analytics')  }}">{{ __('Analytics') }}</a>
        </li>
        <li class="breadcrumb-item">
          <a href="{{ route('photos.index')  }}">{{ __('Photos') }}</a>
        </li>
        <li class="breadcrumb-item active">Photo</li>
      </ol>
      <!-- END Breadcrumbs-->

    
  <form method="POST" action="{{ route('photos.store') }}" enctype="multipart/form-data" >
        @csrf
        @method('PUT')



      @isset($categories)
      
   <p> $categories is defined and is not null... </p>
      @endisset

      @empty($categories)
    <p>   // $categories is "empty"... </p>
      @endempty


       @include('backend.photos._photo_form') 


  </form> 


  </div>
</div>
  <!-- /.container-fluid-->
  <!-- /.content-wrapper-->
  
@endsection

    


		