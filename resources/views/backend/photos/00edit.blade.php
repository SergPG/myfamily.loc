@extends('backend.layouts.backend')

@section('content')


  <div class="content-wrapper">
    <div class="container-fluid">

      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="{{ route('analytics')}}"> {{ __('Analytics') }} </a>
        </li>
        <li class="breadcrumb-item">
          <a href="{{ route('pages.index') }}"> {{ __('Pages')  }}  </a>
        </li>
        <li class="breadcrumb-item active"> {{ __('Edit Page') }}  </li>
      </ol>

     


 <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> {{ __('Page '.$page->name ) }}
        </div>

         <div class="card-body">  

<div class="row">

 <div class="col-9">

     <!-- TAB General   -->
    <div class="tab-content" id="v-pills-tabContent">

      <div class="tab-pane fade show active" id="v-pills-general" role="tabpanel" aria-labelledby="v-pills-general-tab">

        <!-- Form   -->

    <form method="POST" action="{{ route('pages.update',$page) }}" enctype="multipart/form-data" >
        @csrf
        {{ method_field('PUT') }}

      <div class="form-group row">
          <label for="heading" class="col-md-4 col-form-label text-md-right">{{ __('Heading') }}</label>

          <div class="col-md-6">
              <input id="heading" type="text" class="form-control @error('heading') is-invalid @enderror" name="heading" value="{{ $page->heading }}" required autocomplete="heading" autofocus>

              @error('heading')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>
      </div>

      <div class="form-group row">
          <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name item Menu') }}</label>

          <div class="col-md-6">
             <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $page->name }}" required >
  
              @error('name')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>
      </div>

       <div class="form-group row">
          <label for="order_menu" class="col-md-4 col-form-label text-md-right">{{ __('Order in Menu') }}</label>

          <div class="col-md-6">
              <input type="range" id="order_menu" name="order_menu" min="1" max="5" value="{{ $page->order_menu }}" class=" form-control-range form-control @error('order_menu') is-invalid @enderror" name="order_menu">
  
              @error('order_menu')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>
      </div>

       <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-primary">
                    {{ __('Save') }}
                </button>
            </div>
        </div>


      </form>
 
      </div> <!-- END TAB General   -->
  

       <!--  TAB Description   -->
      <div class="tab-pane fade" id="v-pills-description" role="tabpanel" aria-labelledby="v-pills-description-tab">

         <form method="POST" action="{{ route('pages.update',$page) }}" enctype="multipart/form-data" >
        @csrf
        {{ method_field('PUT') }}

      <div class="form-group row">
          <label for="description" class="col-md-3 col-form-label text-md-right">{{ __('Description') }}</label>

          <div class="col-md-8">
              <textarea  id="description" class="form-control @error('description') is-invalid @enderror" name="description" rows="5" >{{ $page->description }}</textarea>

              @error('description')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>
      </div>
      <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-3">
                <button type="submit" class="btn btn-primary">
                    {{ __('Save') }}
                </button>
            </div>
        </div>
     </form>

      </div>   <!-- END TAB Description   -->

       <!-- TAB Slider   -->
      <div class="tab-pane fade" id="v-pills-slider" role="tabpanel" aria-labelledby="v-pills-slider-tab">


 
      <p>
         <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#form_add_slide_item" aria-expanded="false" aria-controls="form_add_slide_item">  {{ __('Add New Slide') }} </button>
      </p>

    

  <div id="form_add_slide_item"  class="row border border-primary  mx-2 my-2 bg-light text-dark collapse ">
       

        <div class="col-md-6 offset-md-3 my-2">
           <img  id="image_file"  class="img-thumbnail" src="{{asset('storage/img/portfolio/2.jpg') }}" alt="" >
        </div>   

        <div class="col-md-10 offset-md-1 my-2">

  @if(isset($slider) && ($slider->count()) > 0)

       <form method="POST" action="{{ route('sliders.update',$slider) }}" enctype="multipart/form-data" >
            {{ method_field('PUT') }}

  @else  

        <form method="POST" action="{{ route('sliders.store') }}" enctype="multipart/form-data" >

  @endif  


          @csrf

            <input type="hidden" name="page" value="{{$page->id}}">
        
          <div class="form-group">
            <label for="heading">{{ __('Image for slide') }}</label>
         
            <input id="upload_img" type="file" class="form-control-file"  name="upload_img" required autocomplete="upload_img">

                  @error('upload_img')
                    <p><span style="margin-top: .25rem; font-size: .875rem; color: #dc3545;">
                      <strong>{{ $message }}</strong>
                    </span></p>
                  @enderror
          </div>

          <div class="form-group">
            <label for="heading">{{ __('Heading') }}</label>
         
           <input id="heading" type="text" class="form-control @error('heading') is-invalid @enderror" name="heading" value="{{ old('heading') }}" autocomplete="heading">

              @error('heading')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>


          <div class="form-group">
            <label for="description">{{ __('Description') }}</label>
         
            <textarea  id="description" class="form-control @error('description') is-invalid @enderror" name="description" rows="3" >{{ old('description') }}</textarea>

              @error('description')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>

        
          <div class="form-group my-1">
            
                <button type="submit" class="btn btn-primary">
                    {{ __('Save') }}
                </button>
                 <button type="reset" class="btn btn-secondary">
                    {{ __('Cansel') }}
                </button>
            
          </div>
     </form>

        </div> 
      </div> <!--  /.row   --> 

    <!--   ////////-------------- -->


      
   

 
 @if(isset($slider) && ($slider->count()) > 0)


      



      <div class="card card-body">
      <div class="row">

@foreach( $slider->slide_items as $slide_item )

       {{-- ////***************  --}}


<div id="form_edit_slide_item_{{ $slide_item->id }}"  class="row border border-primary  mx-2 my-2 bg-light text-dark collapse ">
       

        <div class="col-md-6 offset-md-3 my-2">
           <img  id="file_edit"  class="img-thumbnail" src="{{asset( $slide_item->image ) }}" alt="" >
        </div>   

        <div class="col-md-10 offset-md-1 my-2">

  @if(isset($slider) && ($slider->count()) > 0)

       <form method="POST" action="{{ route('slides.update',$slide_item) }}" enctype="multipart/form-data" >
            {{ method_field('PUT') }}

  @else  

        
  @endif  


          @csrf

            <input type="hidden" name="page" value="{{$page->id}}">
        
          <div class="form-group">
            <label for="heading">{{ __('Image for slide EDIT') }}</label>
         
            <input id="upload_img" type="file" class="form-control-file"  name="upload_img" required autocomplete="upload_img">

                  @error('upload_img')
                    <p><span style="margin-top: .25rem; font-size: .875rem; color: #dc3545;">
                      <strong>{{ $message }}</strong>
                    </span></p>
                  @enderror
          </div>

          <div class="form-group">
            <label for="heading">{{ __('Heading') }}</label>
         
           <input id="heading" type="text" class="form-control @error('heading') is-invalid @enderror" name="heading" value="{{ $slide_item->heading }}" autocomplete="heading">

              @error('heading')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>


          <div class="form-group">
            <label for="description">{{ __('Description') }}</label>
         
            <textarea  id="description" class="form-control @error('description') is-invalid @enderror" name="description" rows="3" >{{ $slide_item->description }}</textarea>

              @error('description')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>

        
          <div class="form-group my-1">
            
                <button type="submit" class="btn btn-primary">
                    {{ __('Save') }}
                </button>
                 <button type="reset" class="btn btn-secondary">
                    {{ __('Cansel') }}
                </button>
            
          </div>
     </form>

        </div> 
      </div> <!--  /.row   --> 



        <div class="col-md-3 my-1">

         <a data-toggle="collapse" href="#form_edit_slide_item_{{ $slide_item->id }}"  aria-expanded="false" aria-controls="form_edit_slide_item_{{ $slide_item->id }}">
  
           <img class="img-thumbnail" src="{{asset($slide_item->image) }}" alt="" >
          </a>

        </div>






        @endforeach 
     
      </div> <!--  /.row   -->
      
     
      
      </div> <!--  /.card card-body   -->
          

      @else
          <p>  Not Slide </p> 

       @endif 


      </div> <!-- END TAB Slider   -->
      
    </div>
  </div>

  <div class="col-3">
    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">

      <a class="nav-link active" id="v-pills-general-tab" data-toggle="pill" href="#v-pills-general" role="tab" aria-controls="v-pills-general" aria-selected="true">General</a>

      <a class="nav-link" id="v-pills-description-tab" data-toggle="pill" href="#v-pills-description" role="tab" aria-controls="v-pills-description" aria-selected="false">Description</a>

      <a class="nav-link" id="v-pills-slider-tab" data-toggle="pill" href="#v-pills-slider" role="tab" aria-controls="v-pills-slider" aria-selected="false">Slider</a>

      <a class="nav-link" id="v-pills-go_to_pages-tab" href="{{ route('pages.index') }}" role="tab" aria-controls="v-pills-go_to_pages" aria-selected="false">Go to Pages </a>
    </div>
  </div>
 
</div>
 </div> <!-- /.card-body -->
 </div><!-- /.card -->



    </div>
  </div>
  <!-- /.container-fluid-->
  <!-- /.content-wrapper-->
  
@endsection

    
 @push('script_Change_Image')

  <script src="{{asset('backend')}}/js/change-image.js"></script>
  
@endpush

		