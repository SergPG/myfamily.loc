@extends('backend.layouts.backend')

@section('content')


<div class="content-wrapper">
  <div class="container-fluid">

      <!-- Breadcrumbs-->
       <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="{{ route('analytics')  }}">{{ __('Analytics') }}</a>
        </li>
        <li class="breadcrumb-item active">Photos</li>
      </ol>
      <!-- END Breadcrumbs-->
      

  <!-- Photos DataTables Card-->
  <div class="card mb-3">

    <div class="card-header">
      <div class="my-1 clearfix">
        <div class="float-left ">  
            <a id="btnEdit" class="btn btn-success float-left ml-3" href="{{ route('photos.create') }}" title="Add" role="button"><i class="mr-1 fa fa-photo fa-lg" aria-hidden="true"></i> {{ __('Add Photo') }}</a>
        </div>
      </div>
    </div>  <!-- /.card-header -->

    <div class="card-body">  

        <nav class="nav-photos">
          <div class="nav nav-tabs" id="nav-tab" role="tablist">

            <a class="nav-item nav-link active" id="nav-all-photos-tab" data-toggle="tab" href="#nav-all-photos" role="tab" aria-controls="nav-all-photos" aria-selected="true">All Photos</a>

            
            @if(isset($categories) && ($categories->count() > 0))
               
              @foreach ( $categories as $category )

                 <a class="nav-item nav-link" id="nav-{{ $category->name }}-tab" data-toggle="tab" href="#nav-{{ $category->name }}" role="tab" aria-controls="nav-{{ $category->name }}" aria-selected="false">{{ $category->heading }}</a>

              @endforeach 

            @endif

            
          </div> <!-- /.nav nav-tabs -->
        </nav> <!-- /.nav-photos -->

        <div class="tab-content" id="nav-tabContent">

           <div class="tab-pane fade show active" id="nav-all-photos" role="tabpanel" aria-labelledby="nav-all-photos-tab">

                

        @if(isset($photos) && ($photos->count() > 0))

          <div class="row my-1">

           @foreach ( $photos as $photo )

                 @include('backend.photos._photo_card') 

            @endforeach 

             </div> <!-- /.row --> 

        @else

           <div class="alert alert-warning mx-3 my-5" role="alert">
              {{ __('No data !') }}
        </div>

        @endif  
             
           </div>  <!-- /.tab-pane -->



          @if(isset($categories) && ($categories->count() > 0))
               
            @foreach ( $categories as $category )

            <div class="tab-pane fade" id="nav-{{ $category->name }}" role="tabpanel" aria-labelledby="nav-{{ $category->name }}-tab">

                 @if(isset($photos) && ($photos->count() > 0))

          <div class="row my-1">

           @foreach ( $photos as $photo )

                 @include('backend.photos._photo_card') 

            @endforeach 

             </div> <!-- /.row --> 

        @else

           <div class="alert alert-warning mx-3 my-5" role="alert">
              {{ __('No data !') }}
        </div>

        @endif  

            </div>  <!-- /.tab-pane -->  

            @endforeach 

          @endif  
      
      </div>  <!-- /#nav-tabContent -->
    </div> <!-- /.card-body -->


      <meta name="_token" content="{!! csrf_token() !!}" />

      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
  
@endsection

    
@push('script_Pages')



  <script type="text/javascript">

// DESC  //  ASC
  $(document).ready(function(){
      
     
    //delete Slide and remove it from list
    $(document).on('click','#btnDelete',function(){
        
        var page_id = $(this).val();
        var url = $(this).data("url");

        console.log(url, page_id);
        
    //     debugger;
       
        if(confirm("ВЫ ХОТИТЕ УДАЛИТЬ ЗАПИСЬ "+ page_id + " ?"))
        {
             $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })
            $.ajax({
                type: "DELETE",
                url: url,
                processData : false,
                contentType : false,
                success: function (data) {
                    console.log(data);
                    $("#page_" + page_id).remove();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
         }; // end if
       });


    

  });
    
  </script>

  
@endpush

		