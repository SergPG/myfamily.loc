@extends('backend.layouts.backend')

@section('content')

<div class="content-wrapper">
  <div class="container-fluid">


     <!-- Breadcrumbs-->
       <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="{{ route('analytics')  }}">{{ __('Analytics') }}</a>
        </li>
        <li class="breadcrumb-item">
          <a href="{{ route('photos.index')  }}">{{ __('Photos') }}</a>
        </li>
        <li class="breadcrumb-item active">Photo</li>
      </ol>
      <!-- END Breadcrumbs-->





 <div class="  my-1 ">

  <div class="card card-body">
   <div class="row">
    <div class="col-3">
      <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseFormAddCategory" aria-expanded="false" aria-controls="collapseFormAddCategory"> {{ __('Add Category')  }}</button>
      
    </div>  {{-- /.col-3 --}}
    <div class="col-9 collapse" id="collapseFormAddCategory">
      
        <form method="POST" action="{{ route('photos.store') }}" class="form-inline">
           @csrf
          
          <div class="form-group mx-sm-3">
            <label for="add_category" class="sr-only">Add Category</label>
            <input type="text" class="form-control" id="add_category" placeholder="Category Name">
          </div>
          <button type="submit" class="btn btn-primary">OK</button>
        </form>

    </div> {{-- /.col-9 --}}
   </div> {{-- /.row --}}


<div class=" mb-2 ">
  
</div>

<div class="collapse " id="collapseExample">
  <div class="">

     

  </div>
</div>

</div>

</div>


  
    
  <form method="POST" action="{{ route('photos.store') }}" enctype="multipart/form-data" >
        @csrf
      
    

       @include('backend.photos._photo_form') 


  </form> 


  </div>
</div>
  <!-- /.container-fluid-->
  <!-- /.content-wrapper-->
  
@endsection

    


		