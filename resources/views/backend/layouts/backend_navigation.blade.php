




  <!-- Navigation-->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="{{ route('home') }}">Go To Site</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarResponsive">

    {{-- =============  menuMainAdmin  =================   --}}

       {!! $menuMainAdmin->asUl(
              ['class' => 'navbar-nav navbar-sidenav',
               'id'=>'exampleAccordion']);
        !!}


     {{--  <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">

        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Analytics">
          <a class="nav-link" href="{{ route('analytics')  }}">
            <i class="fa fa-lg fa-fw fa-dashboard"></i>
            <span class="nav-link-text">Analytics</span>
          </a>
        </li>

        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Pages">
          <a class="nav-link" href="Pages.html">
            <i class="fa fa-lg fa-fw fa-file"></i>
            <span class="nav-link-text">Pages</span>
          </a>
        </li>
        
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Posts">
          <a class="nav-link" href="Posts.html">
            <i class="fa  fa-lg fa-fw fa-edit"></i>
            <span class="nav-link-text">Posts</span>
          </a>
        </li>

        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Gallery">
          <a class="nav-link" href="Gallery.html">
            <i class="fa fa-lg fa-fw fa-image"></i>
            <span class="nav-link-text">Gallery</span>
          </a>
        </li>

        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Sliders">
          <a class="nav-link" href="Sliders.html">
            <i class="fa fa-lg fa-fw fa-clone"></i>
            <span class ="nav-link-text">Sliders</span>
          </a>
        </li>

        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Settings">
          <a class="nav-link" href="Settings.html">
            <i class="fa fa-lg fa-fw fa-wrench"></i>
            <span class="nav-link-text">Settings</span>
          </a>
        </li>
  
      </ul> --}}

  {{-- ============= END menuMainAdmin  =================     --}} 




      <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item">
          <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
          </a>
        </li>
      </ul>


      <ul class="navbar-nav ml-auto">
        
        <li class="nav-item">
          <form class="form-inline my-2 my-lg-0 mr-lg-2">
            <div class="input-group">
              <input class="form-control" type="text" placeholder="Search for...">
              <span class="input-group-btn">
                <button class="btn btn-primary" type="button">
                  <i class="fa fa-search"></i>
                </button>
              </span>
            </div>
          </form>
        </li>

        <li class="nav-item">
          <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
            <i class="fa fa-fw fa-sign-out"></i>Logout</a>
        </li>
      </ul>
    </div>
  </nav>