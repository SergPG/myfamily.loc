<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
  /**
   * Связанная с моделью таблица.
   * @var string
   */
  protected $table = 'posts';

  public function slider()
    {
        return $this->belongsTo('App\Models\Slider','sliders_id','id');
    }

  public function page()
    {
        return $this->belongsTo('App\Models\Page','sliders_id','id');
    }



}
