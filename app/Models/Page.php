<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    
  /**
   * Связанная с моделью таблица.
   * @var string
   */
  protected $table = 'pages';

  public function slider()
    {
        return $this->belongsTo('App\Models\Slider','sliders_id','id');
    }
  
  public function posts()
    {
        return $this->hasMany('App\Models\Post','sliders_id','id');
    }


}
