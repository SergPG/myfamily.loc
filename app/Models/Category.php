<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

  /**
   * Связанная с моделью таблица.
   * @var string
   */
  protected $table = 'categories';

  public function photos()
    {
      return $this->hasMany('App\Models\Photo','category_id','id');
    }




}
