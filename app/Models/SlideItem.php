<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SlideItem extends Model
{
    //
     /**
   * Связанная с моделью таблица.
   * @var string
   */
  protected $table = 'slide_items';

  public function slider()
    {
        return $this->belongsTo('App\Models\Slider','sliders_id','id');
    }




}
