<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
  /**
   * Связанная с моделью таблица.
   * @var string
   */
  protected $table = 'photos';
  

  public function category()
    {
        return $this->belongsTo('App\Models\Category','category_id','id');
    }



}
