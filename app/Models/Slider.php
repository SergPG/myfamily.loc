<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
  /**
   * Связанная с моделью таблица.
   * @var string
   */
  protected $table = 'sliders';

   public function page()
    {
        return $this->hasOne('App\Models\Page','sliders_id','id');
    }

	public function post()
    {
        return $this->hasOne('App\Models\Post','sliders_id','id');
    }

	public function slide_items()
    {
        return $this->hasMany('App\Models\SlideItem','sliders_id','id');
    }



  
}
