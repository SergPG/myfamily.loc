<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\FrontendController;

use App\Models\Page;


class PagesController extends FrontendController
{
    //
   public function __construct(){
		parent :: __construct();	
	}


   public function home()
    {
       

     $page = Page::where('template', 'home')->firstOrFail();
     $this->vars = array_add($this->vars,'page',$page); 

     $slider = $page->slider;

     if(isset($slider)){
        $slide_items = $slider->slide_items;
        $this->vars = array_add($this->vars,'slide_items',$slide_items); 
       // dd($slide_items);
     }     

     return view('frontend.pages.home')->with($this->vars);   

    }


    public function show($name = null)
    {
         
    if(isset($name)){     
       $page = Page::where('template', $name)->firstOrFail();
        $this->vars = array_add($this->vars,'page',$page); 

       $slider = $page->slider;

          if(isset($slider)){
            $slide_items = $slider->slide_items;
            $this->vars = array_add($this->vars,'slide_items',$slide_items); 
             //  dd($slide_items);
             }
          else{
            $slide_items = null;
            $this->vars = array_add($this->vars,'slide_items',$slide_items); 
          }    

    }

         return view('frontend.pages.'.$name)->with($this->vars);

    }


    public function send(Request $request)
    {
       echo "string";
    }
    
}
