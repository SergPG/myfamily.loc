<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Menu;
use App\Models\Page;

class FrontendController extends Controller
{
    //
    protected $vars = array();
    protected $pages;

    public function __construct(){

       $this->init();

	   }

     /*
        Init Page and Main Menu
     */  
      public function init(){
     
    //  $pages = Page::orderBy('order_menu', 'desc')->get();
      $pages = Page::orderBy('order_menu', 'asc')->get();

      if( isset($pages) && ($pages->count()) > 0  ){

         $menuMain = $this->getMenuMain($pages); 
      
         $this->vars = array_add($this->vars,'menuMain',$menuMain); 
          }
      else {
           $config_pages = config('pages.pages');

           foreach ($config_pages as $key => $value) {
               
               $page = new Page;

               $page->name = $value['name'];
               $page->heading = $value['heading'];
               $page->template = $value['template'];
               $page->order_menu = $value['order_menu'];

               $page->save();
             } // End Foreach

            $pages = Page::orderBy('order_menu', 'asc')->get();

            $menuMain = $this->getMenuMain($pages); 
      
            $this->vars = array_add($this->vars,'menuMain',$menuMain); 


        } // End If Else

      } // End Init()



  public function getMenuMain( $pages ){

    $this->pages =  $pages;

  	$result = Menu::make('MenuMain', function ($m) {

        foreach ($this->pages as $key => $page) {

          if($page->template === 'home' ){
              $m->add($page->name,['route'  => 'home']);
          }
           else{
              $m->add( $page->name,['route'  => ['pages', 'name' => $page->template]]);
           }  // End If Else
        } // End Foreach 
  
  	}); // Menu::make()
       // dd($result);

  	return $result;
  }  // END getMenuMain()


}
