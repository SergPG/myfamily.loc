<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\FrontendController;

class GalleryController extends FrontendController
{
    //
   public function __construct(){
		parent :: __construct();	
	}

   public function index()
    {
        //
      // dd($this->vars);
         return view('frontend.gallery.index')->with($this->vars);

    }
}
