<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Menu;
use App\Models\Page;

class BackendController extends Controller
{
    //
    protected $vars = array();

    public function __construct(){

      $this->init();
		
  	$menuMainAdmin = $this->getMenuMainAgmin(); 
  		
  	$this->vars = array_add($this->vars,'menuMainAdmin',$menuMainAdmin); 
		
		//dd($this->vars);	
	}
   
    public function init(){
     
      $pages = Page::all();
      if( isset($pages) && ($pages->count()) > 0  ){

       // dd($pages);

      }
      else {
           $config_pages = config('pages.pages');

           foreach ($config_pages as $key => $value) {
               
               $page = new Page;

               $page->name = $value['name'];
               $page->heading = $value['heading'];
               $page->template = $value['template'];
               $page->order_menu = $value['order_menu'];

               $page->save();
           } // End Foreach
      } // End Else



    }


    public function getMenuMainAgmin(){

	$result = Menu::make('MenuMainAdmin', function ($m) {
      $m->add('<i class="fa fa-fw fa-dashboard"></i>
            <span class="nav-link-text">Analytics</span>',
            	['route'  => 'analytics',
      			 'class' => 'nnav-item',
      			 'data-toggle' => 'tooltip',
      			 'data-placement' => 'right',
      			 'title' => 'Analytics',
      		 	 ])->link->attr(['class' => 'nav-link']);

	  $m->add('<i class="fa fa-fw fa-file"></i>
            <span class="nav-link-text">Pages</span>',
            	['route'  => 'pages.index',
      			 'class' => 'nnav-item',
      			 'data-toggle' => 'tooltip',
      			 'data-placement' => 'right',
      			 'title' => 'Pages',
      		 	 ])->link->attr(['class' => 'nav-link']);

    $m->add('<i class="fa fa-fw fa-photo"></i>
            <span class="nav-link-text">Photos</span>',
              ['route'  => 'photos.index',
             'class' => 'nnav-item',
             'data-toggle' => 'tooltip',
             'data-placement' => 'right',
             'title' => 'Photos',
             ])->link->attr(['class' => 'nav-link']);

   

     // $m->add('Posts',['route'  => ['posts']]);

   //  $m->add('Gallery',['route'  => 'photos']);
    //  $m->add('Settings',['route'  => 'settings']);
	});

	return $result;
    }
 
     /*
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Analytics">
          <a class="nav-link" href="{{ route('analytics')  }}">
            <i class="fa fa-lg fa-fw fa-dashboard"></i>
            <span class="nav-link-text">Analytics</span>
          </a>
        </li>

     */



}
