<?php

namespace App\Http\Controllers\Backend;

use App\Models\Photo;
use App\Models\Category;

use Illuminate\Support\Facades\Storage;

use Illuminate\Http\Request;
use App\Http\Controllers\Backend\BackendController;

class PhotosController extends BackendController
{
    
    //
    public function __construct(){
        parent :: __construct();    
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        
     $photos = Photo::get();
     $this->vars = array_add($this->vars,'photos',$photos);

     $categories = Category::get();
     $this->vars = array_add($this->vars,'categories',$categories);

$this->vars = array_add($this->vars,'all_categories',true);
      

     
        return view('backend.photos.index')->with($this->vars); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
     $photos = [];
     $this->vars = array_add($this->vars,'photos',$photos);

     $categories = Category::get();
     
     if ( $categories->count() > 0 ) {
         $this->vars = array_add($this->vars,'categories',$categories);
     }
     else{
        $categories = [];
        $this->vars = array_add($this->vars,'categories',$categories);
     }  

     return view('backend.photos.create')->with($this->vars); 


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        echo "string  Store";
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function show(Photo $photo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function edit(Photo $photo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Photo $photo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Photo $photo)
    {
        //
    }
}
