<?php

namespace App\Http\Controllers\Backend;

use App\Models\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Backend\BackendController;

class PagesController extends BackendController
{
    
     //
    public function __construct(){
        parent :: __construct();    
    }

    



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $pages = Page::all();
        $this->vars = array_add($this->vars,'pages',$pages); 

        // $slider = $pages->find(1)->slider;

        // dd($slider);

        return view('backend.pages.index')->with($this->vars);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //




        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
        //
        $this->vars = array_add($this->vars,'page',$page); 

        $slider = $page->slider; 
        $this->vars = array_add($this->vars,'slider',$slider); 

       // dd($slider);
        
        return view('backend.pages.edit')->with($this->vars);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Page $page)
    {
        //
       if ($request->has('description') ) {
  //            
             $page->description = $request->description;
             $page->save();

             return back()->with('status', 'Updata OK!');
           
        }
        else{

            $this->validate($request, [
                'heading' => 'required|max:255',
                'name' => 'required|max:255',
                'order_menu' => 'required',
              ]);
            
            $page->heading = e($request->heading);
            $page->name = e($request->name);
            $page->order_menu = e($request->order_menu);

            $page->save();

            return back()->with('status', 'Updata OK!');


            
        }

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        //
    }
}
