<?php

namespace App\Http\Controllers\Backend;

use App\Models\Slider;
use App\Models\SlideItem;
use App\Models\Page;
use App\Models\Post;

use Illuminate\Support\Facades\Storage;

use Illuminate\Http\Request;
use App\Http\Controllers\Backend\BackendController;

class SlidersController extends BackendController
{
    

     protected  $path_image = '/public/slides';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        // --------------------
        $slider = new Slider;
     
        // ----------------------

        $max = $slider->max('id');

        $slider->name = 'slider_'.($max + 1);

        $slider->save();


        if ($request->has('page')) {
            $page = Page::find($request->page);
            $page->sliders_id = $slider->id;
            $page->save();
        }
        
        if ($request->has('post')) {
            $post = Post::find($request->post);
            $post->sliders_id = $slider->id;
            $post->save();
        }
       
        // ----------------------
     
        $this->add_slide_item($request, $slider);

         return back()->with('status', 'Updata OK!');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function show(Slider $slider)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function edit(Slider $slider)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Slider $slider)
    {
       
           $this->add_slide_item($request, $slider);
        
        $slider->save();
         return back()->with('status', 'Updata OK!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function destroy(Slider $slider)
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function add_slide_item(Request $request, Slider $slider)
    {
        // --------------------
        $slide_item = new SlideItem;
        // ----------------------

        $this->validate($request, [
              //  'heading' => 'max:255',
                'upload_img' => 'required|image',
              ]);

        //-------
        $upload_img = $request->file('upload_img'); 
        $ext_img = $upload_img->getClientOriginalExtension();

        $slide_img = str_random(20).'.'.$ext_img;

        $url_img = $upload_img->storeAs($this->path_image,$slide_img);
             
        $url_slide_img = Storage::url($url_img);

        //-------------
        $slide_item->sliders_id = $slider->id;
         
        $slide_item->image = $url_slide_img;


         if ($request->has('heading')) {
                $slide_item->heading = e($request->heading);
            }
            
            if ($request->has('description')) {
                $slide_item->description = e($request->description);
            }

        $slide_item->save();
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function update_slide(Request $request, SlideItem $slide_item)
    {
       
            $this->validate($request, [
                'upload_img' => 'image',
              ]);
           
            //-------
            //   dd($old_url_img);

           if ($request->hasFile('upload_img')) {

               $old_url_img = $slide_item->image;

               $pos =  strpos($old_url_img, '/',1); 
               // "/storage/slides/slide_17.jpg"

               $puth = substr($old_url_img,  ($pos + 1));
               // "slides/slide_17.jpg"

                $exists = Storage::disk('public')->exists( $puth );

                 if( $exists ){
                    $delfiles = Storage::disk('public')->delete( $puth );
                  }
   

                $upload_img = $request->file('upload_img'); 
                $ext_img = $upload_img->getClientOriginalExtension();

                $slide_img = str_random(20).'.'.$ext_img;

                $url_img = $upload_img->storeAs($this->path_image,$slide_img);
                 
                $url_slide_img = Storage::url($url_img);

                $slide_item->image = $url_slide_img;
            } 
            //---END if()  ---------

            if ($request->has('heading')) {
                $slide_item->heading = e($request->heading);
            }

            if ($request->has('description')) {
                $slide_item->description = e($request->description);
            }

            $slide_item->save();

             return back()->with('status', 'Updata OK!');

    }




}
