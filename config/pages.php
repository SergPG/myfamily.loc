<?php

return [
    
       'pages' => [
       	   'page_1' => [
       	   		'name' => 'home',
       	   		'heading' => 'Home',
       	   		'template' => 'home',
       	   		'order_menu' => '1',

       	    ],
       	    'page_2' => [
       	   		'name' => 'story',
       	   		'heading' => 'Oue Story',
       	   		'template' => 'story',
       	   		'order_menu' => '2',
       	    ],
 			'page_3' => [
       	   		'name' => 'events',
       	   		'heading' => 'Events',
       	   		'template' => 'events',
       	   		'order_menu' => '3',
       	    ],
			'page_4' => [
       	   		'name' => 'gallery',
       	   		'heading' => 'Gallery',
       	   		'template' => 'gallery',
       	   		'order_menu' => '4',
       	    ],
			'page_5' => [
       	   		'name' => 'contact',
       	   		'heading' => 'Contact',
       	   		'template' => 'contact',
       	   		'order_menu' => '5',
       	    ],
       ],
   
];
